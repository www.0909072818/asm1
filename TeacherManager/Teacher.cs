﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManager
{
    internal class Teacher : Worker
    {
        public double CoefficientsSalary { get; set; }
        public Teacher() : base() { }
        public Teacher(String name, int YearOfBirth, double basicSalary, double coefficientsSalary) : base(name, YearOfBirth, basicSalary)
        {
            base.Name = name;
            base.YearOfBirth = YearOfBirth;
            base.BasicSalary = basicSalary;
            this.CoefficientsSalary = coefficientsSalary;
        }
        public void InputInfo(double coefficientsSalary)
        {
            CoefficientsSalary = coefficientsSalary;
        }
        public new double CalculateSalary()
        {
            return base.BasicSalary * CoefficientsSalary * 1.25f;
        }
        public void OutputInfo()
        {
            Console.WriteLine($"Name:{base.Name},Year of Birth:{base.YearOfBirth},Basic salary:{base.BasicSalary},Coefficients Salary:{CoefficientsSalary},Salary:{CalculateSalary()}");
        }
        public double Processing()
        {
            return CoefficientsSalary += 0.6f;
        }
    }
}
