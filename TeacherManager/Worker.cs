﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManager
{
    internal class Worker
    {
        public string Name { get; set; }
        public int YearOfBirth { get; set; }
        public double BasicSalary { get; set; }

        public Worker() { }
        public Worker(String name,int yearOfBirth, double basicSalary)
        {
            this.Name = name;
            this.YearOfBirth = yearOfBirth;
            this.BasicSalary = basicSalary;
        }
        public void InputInfo()
        {
            Console.Write("Name:");
            Name = Console.ReadLine();
            Console.Write("Year of birth:");
            YearOfBirth = Convert.ToInt16(Console.ReadLine());
            Console.Write("Basic salary:");
            BasicSalary = Convert.ToDouble(Console.ReadLine());
        }
        public double? CalculateSalary()
        {
            return BasicSalary;
        }
        public void OuputInfo()
        {
            Console.WriteLine($"Name:{Name},Year of birth:{YearOfBirth},Basic salary:{BasicSalary}");
        }
    }
}
