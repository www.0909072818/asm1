﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManager
{
    public static class Tools
    {
        public static String checkStringNotBlank(string mgs)
        {
            string value;
            while (true)
            {
                try
                {
                    Console.Write(mgs);
                    value = Console.ReadLine();
                    if (value.Trim().Length > 0 )
                    {
                        break;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }catch (Exception e)
                {
                    Console.WriteLine("Name can't be blank!");
                }
            }
            return value;
        }
        public static int checkYob(string mgs)
        {
            int value;
            while(true)
            {
                try
                {
                    Console.Write(mgs);
                    value = Convert.ToInt32(Console.ReadLine());
                    if(value > 0 && value < 9999)
                    {
                        break;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong format!");
                }
            }
            return value;
        }
        public static double checkSalary(string mgs)
        {
            double value;
            while (true)
            {
                try
                {
                    Console.Write(mgs);
                    value = Convert.ToDouble(Console.ReadLine());
                    if (value > 0 && value < 9999999)
                    {
                        break;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    Console.WriteLine("Wrong format!");
                }
            }
            return value;
        }
    }
}
