﻿using System.ComponentModel;
using TeacherManager;

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Enter number of teacher:");
        int numberOfTeacher = Convert.ToInt32(Console.ReadLine());
        List<Teacher> teachers = new List<Teacher>();
        for (int i = 0; i < numberOfTeacher; i++)
        {
            Console.WriteLine($"Enter info if teacher {i + 1}:");
            String Name = Tools.checkStringNotBlank("Name:");
            int Yob = Tools.checkYob("Year of Birth:");
            double basicSalary = Tools.checkSalary("Basic salary:");
            double coefficientsSalary = Tools.checkSalary("Coefficients Salary:");
            Teacher teacher = new Teacher(Name, Yob, basicSalary, coefficientsSalary);
            teachers.Add(teacher);
        }
        teachers.OrderByDescending(x => x.CalculateSalary());
        Console.WriteLine("\nList of teacher");
        foreach (Teacher teacher in teachers)
        {
            teacher.OutputInfo();
        }
        Console.Write("\nLowest salary teacher:");
        teachers.FirstOrDefault().OutputInfo();
        Console.ReadKey();
    }
}